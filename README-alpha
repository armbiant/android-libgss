GSS README-alpha -- Information for developers.                 -*- outline -*-
Copyright (C) 2009-2022 Simon Josefsson
See the end for copying conditions.

This file contains instructions for developers and advanced users that
wants to build from version controlled sources.

We require several tools to build the software, including:

- Automake <https://www.gnu.org/software/automake/>
- Autoconf <https://www.gnu.org/software/autoconf/>
- Libtool <https://www.gnu.org/software/libtool/>
- Gettext <https://www.gnu.org/software/gettext/>
- Texinfo <https://www.gnu.org/software/texinfo/>
- help2man <https://www.gnu.org/software/help2man/>
- Gengetopt <https://www.gnu.org/software/gengetopt/>
- Tar <https://www.gnu.org/software/tar/>
- Gzip <https://www.gnu.org/software/gzip/>
- Texlive & epsf <https://www.tug.org/texlive/> (for PDF manual)
- CVS <https://www.gnu.org/software/cvs/> (for gettext autopoint)
- GTK-DOC <https://www.gtk.org/gtk-doc/> (for API manual)
- Git <https://git.or.cz/>
- Perl <https://www.cpan.org/>
- Valgrind <https://valgrind.org/> (optional)
- Shishi <https://www.gnu.org/software/shishi/> (optional)

The required software is typically distributed with your operating
system, and the instructions for installing them differ.  Here are
some hints:

gNewSense/Debian/Ubuntu:
sudo apt-get install git-core autoconf automake libtool gettext cvs
sudo apt-get install texinfo texlive texlive-generic-recommended texlive-extra-utils
sudo apt-get install help2man gtk-doc-tools valgrind gengetopt
sudo apt-get install libshishi-dev

To download the version controlled sources:

$ git clone https://git.savannah.gnu.org/git/gss.git
$ cd gss

Bootstrap it as follows:

$ ./bootstrap
$ ./configure

Then build the project normally:

$ make
$ make check

Happy hacking!

----------------------------------------------------------------------
Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
