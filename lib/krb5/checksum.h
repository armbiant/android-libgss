/* krb5/checksum.h --- (Un)pack checksum fields in Krb5 GSS contexts.
 * Copyright (C) 2003-2022 Simon Josefsson
 *
 * This file is part of the GNU Generic Security Service Library.
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at
 *    your option) any later version.
 *
 * or
 *
 * * the GNU General Public License as published by the Free Software
 *   Foundation; either version 2 of the License, or (at your option)
 *   any later version.
 *
 * or both in parallel, as here.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License
 * and the GNU Lesser General Public License along with this file.  If
 * not, see <http://www.gnu.org/licenses/>.
 *
 */

OM_uint32
_gss_krb5_checksum_pack (OM_uint32 * minor_status,
			 const gss_cred_id_t initiator_cred_handle,
			 gss_ctx_id_t * context_handle,
			 const gss_channel_bindings_t input_chan_bindings,
			 OM_uint32 req_flags, char **data, size_t *datalen);

OM_uint32
_gss_krb5_checksum_parse (OM_uint32 * minor_status,
			  gss_ctx_id_t * context_handle,
			  const gss_channel_bindings_t input_chan_bindings);
